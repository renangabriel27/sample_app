require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user             = users(:archer)
    @deactivated_user = users(:tom)
  end

  test "should not show user not activeted" do
  	log_in_as (@user)
    get user_path(@deactivated_user)
    assert_redirected_to root_url
  end

end